package com.itmuch.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itmuch.cloud.entity.User;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@RestController
public class MovieConroller {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private EurekaClient eurekaClient;
	
	@GetMapping("/movie/{id}")
	public User findById(@PathVariable Long id){
		return restTemplate.getForObject("http://localhost:7900/simple/"+id, User.class);
	}
	
	
	@GetMapping("/homePage")
	public String serviceUrl() {
	    InstanceInfo instance = eurekaClient.getNextServerFromEureka("MICROSERVICE-CONSUMER-MOVIE", false);
	    return instance.getHomePageUrl();
	}
}
